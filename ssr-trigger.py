#!/bin/python3

from gpiozero import LED, Button
from time import sleep, time_ns
from enum import Enum


class ChangeType(Enum):
    increase = 15  # This number maps to Button GPIO pin number
    decrease = 18


# 3 separate LED segments with assign GPIO pins
segments = {"0": (19, 13, 16, 22, 4, 2, 3), "1": (21, 26, 1, 9, 11, 27, 17), "2": (8, 25, 23, 5, 6, 0, 10)}

# Correct mapping for LED-s pins to numbers representation
num = {
    " ": (0, 0, 0, 0, 0, 0, 0),
    "0": (1, 1, 1, 1, 1, 1, 0),
    "1": (0, 0, 1, 1, 0, 0, 0),
    "2": (1, 1, 0, 1, 1, 0, 1),
    "3": (0, 1, 1, 1, 1, 0, 1),
    "4": (0, 0, 1, 0, 0, 1, 1),
    "5": (0, 1, 1, 0, 1, 1, 1),
    "6": (1, 1, 1, 0, 1, 1, 1),
    "7": (0, 0, 1, 1, 1, 0, 0),
    "8": (1, 1, 1, 1, 1, 1, 1),
    "9": (0, 1, 1, 1, 1, 1, 1),
}

# Mapping for rest on the necessary pins
number = {"dot": 20, "output": 12}

last_update = time_ns()

# Options
step_press = 5  # Value added/substracted (ms) when pressing the button
init_number = 0  # Starting value for LED-s
str_number = "000"  # String representation for 'init_number'
min_update_time = 100000000  # Minimal time delay (ns) required to modyfiy LED-s value (impulse value)


def change_value(button: Button):
    global init_number, str_number, last_update

    now_time = time_ns()

    # Check if enough time has passed to update the value
    if now_time - last_update < min_update_time:
        return

    if button.pin.number == ChangeType.increase.value:
        if init_number + step_press <= 999:
            init_number += step_press
        else:
            return
    elif button.pin.number == ChangeType.decrease.value:
        if init_number - step_press >= 0:
            init_number -= step_press
        else:
            return
    else:
        return

    str_number = numer_to_string(init_number)
    last_update = now_time


def numer_to_string(number):
    str_num = str(number)

    if len(str_num) == 3:
        return str_num
    elif len(str_num) == 2:
        return "0" + str_num
    else:
        return "00" + str_num


def trigger():
    led[number["output"]].on()
    sleep(init_number / 100)
    led[number["output"]].off()


def run_loop():
    while True:
        if button_plus.is_pressed:
            change_value(button_plus)

        if button_minus.is_pressed:
            change_value(button_minus)

        for segment in range(3):
            for num_bin, gpio_num in zip(num[str_number[segment]], segments[str(segment)]):
                if num_bin:
                    led[gpio_num].on()
                else:
                    led[gpio_num].off()
        sleep(0.2)


def init():
    global led, button_plus, button_minus, button_trigger
    # Assing LED-s GPIO pins to LED objects with OFF state (LOW)
    led = {}
    for segment in range(3):
        for gpio_num in segments[str(segment)]:
            led[gpio_num] = LED(gpio_num)
            led[gpio_num].off()

    # Add dot for x.xx representation
    led[number["dot"]] = LED(number["dot"])
    led[number["dot"]].on()

    # Map 3 buttons to Button objects
    button_plus = Button(15)
    button_minus = Button(18)
    button_trigger = Button(14)

    # Add handlers for button press action
    button_plus.when_pressed = change_value
    button_minus.when_pressed = change_value
    button_trigger.when_pressed = trigger

    # LED object that will send trigger signal to RSS
    led[number["output"]] = LED(number["output"])
    led[number["output"]].off()


init()
run_loop()
