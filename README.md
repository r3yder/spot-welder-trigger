# Spot welder trigger

Script that allows to send programmed impulses to SSR relay.

## Requirements

- Raspberry Pi 3 model B (or newer)
- 3x 7 segment 1 bit LED (10 pin)
- 3x button
- Solid State Relay – SSR-40A
- 75307D MOSFET (or similar)

## Cicruit
TODO

## Setup

<img src="images/setup.jpg" width="300" />

## Notes

- SSR relay is rated to open gate from 3V but it is required at least ~3.8V which means that voltage from GPIO pin is not enough and we need to operate on 5V power line which is not configurable by design
